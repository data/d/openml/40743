# OpenML dataset: data-v3.es-de.IRI.clean.anno.uniform.arff

https://www.openml.org/d/40743

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

DBpedia incorrect mapping prediction. Spanish-German-IRIs annotations

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40743) of an [OpenML dataset](https://www.openml.org/d/40743). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40743/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40743/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40743/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

